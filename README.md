# dgx-branch-hours

`dgx-branch-hours` provides a method, `branchHours` that take three inputs, (1)
a list of dates, (2) an object representing an NYPL branch normal hours and (3)
an object containing closing alerts -- the last two from the NYPL Refinery API
-- and returns an object representing, for each date, the expected regular hours of operation and, if any closings apply, the adjusted hours.

## Usage

    branchHours({dates: DATES, regular: HOURS, closings: CLOSINGS});

Given an array of dates in the format 'YYYY-MM-DD', a regular hours object as returned from the Refinery, and an array of alerts as returned by the refinery, for example:

    //
    const theDates = ['2015-12-24'];

    const theHours = [
	  // ... other days skipped for brevity
	  { day: "Thu.", open: "10:30", close: "20:00" },
      { day: "Fri.", open: "10:30", close: "18:00" },
      { day: "Sat.", open: "10:30", close: "18:00" }
    ];

    const theAlerts = [{
      id: "283895",
      scope: "all",
      _links: {
        web: {
          href: "http://www.nypl.org/node/283895"
        }
      },
      msg: "<p>All NYPL locations will close at 3 p.m. on December 24 and will remain closed on December 25.</p> ",
      display: {
        start: "2015-12-17T00:00:00-05:00",
        end: "2015-12-25T23:00:00-05:00"
      },
      closed_for: "Closed for Christmas Day",
      applies: {
        start: "2015-12-24T15:00:00-05:00",
        end: "2015-12-25T23:00:00-05:00"
      }
    };

    branchHours({dates: theDates, regular: theHours, closings: theAlerts});

Would return the following object:

    [
      { dow: 4,
        orig: '2015-12-24',
        hours:
        { regular: { open: [Object], close: [Object] },
          actual: { open: [Object], close: [Object] },
          modified: true,
          id: '283895',
          type: 'closing early',
          msg: 'Closed for Christmas Day'
	}
      }
    ]

The `open` and `close` properties are Moment.js objects.

`branchHours` takes an array of dates and returns an array of objects of the same number in the same order, each with the properties:

* `dow`: Day of the week (0 = Sunday) for your ordering convenience.
* `orig`: The corresponding input date string
* `hours`: Hours calculated for the given date
    * `regular`: The regular hours for the given date
    * `actual`: The actual hours for the given date, taking any closings into account. If there are no changes to the regular hours, this will be identical to `hours.regular`
    * `modified`: true if `actual` differs from `regular`
    * `id`: id of the alter that caused a modification to the hours
    * `type`: One of `closed`, `closing early`, `opening late`
    * `msg`: The message of the alert that caused a modification to the hours.
	  
