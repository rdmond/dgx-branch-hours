import moment from 'moment';

function parseClosing({ applies: { start, end }, id, msg, closed_for} = {}) {
  return {
    start: moment(start),
    end: moment(end),
    id: id,
    msg: msg,
    closed_for: closed_for,
  };
}

/**
 * Take a date with no time and a string representing a time, and combine them
 * into a single datetime. Assumes the time in the format 'HH:MM'.
 * @return {Object} A Moment object, or null if the time is null.
 */
function fullDateTime(date, time) {
  if (time === null) {
    return null;
  }

  const [hour, minutes] = time.split(':').map(
    (n) => { return parseInt(n, 10); }
  );

  return moment(date).hour(hour).minute(minutes);
}

function atOrBefore(a, b) {
  return a.isSame(b) || a.isBefore(b);
}

function atOrAfter(a, b) {
  return a.isSame(b) || a.isAfter(b);
}

/**
 * Closed is defined as having no opening time and no closing time (i.e., both
 * are null). Return an object representing being closed
 *
 */
function closed() {
  return {open: null, close: null};
}

/**
 * Closed is defined as having no opening time and no closing time (i.e., both
 * are null). Return true if this is the case.
 *
 */
function isClosed({open, close} = {}) {
  return (open === null && close === null);
}

/**
 * Return true if the the branch should be closed according to the closing.
 * There are four ways to be closed:
 *
 * <--> = closing
 * |--| = hours
 *
 *
 * <-------------------->
 *      |----------|
 *
 * <--------------->
 *      |----------|
 *
 *      <---------->
 *      |----------|
 *
 *      <--------------->
 *      |----------|
 *
 */
function shouldClose(rg, cl) {
  return (atOrBefore(cl.start, rg.open) && atOrAfter(cl.end, rg.close));
}

/**
 * Return true if the the branch should be open late according to the closing.
 * There are two ways to open late closed:
 *
 * <--> = closing
 * |--| = hours
 *
 *           <---------->
 *      |----------|
 *
 *           <----->
 *      |----------|
 *
 */
function shouldCloseEarly(rg, cl) {
  return (cl.start.isBetween(rg.open, rg.close) && atOrAfter(cl.end, rg.close));
}

/**
 * Return true if the the branch should close early according to the closing.
 * There are four ways to be close early:
 *
 * <--> = closing
 * |--| = hours
 *
 *
 * <---------->
 *      |----------|
 *
 *      <----->
 *      |----------|
 *
 */
function shouldOpenLate(rg, cl) {
  return (atOrBefore(cl.start, rg.open) && cl.end.isBetween(rg.open, rg.close));
}

function calculateClosings(
    {regular, actual, modified=false, id, type, msg} = {},
    [head, ...tail]) {
    const actualH = typeof (actual) === "undefined" ? regular : actual;
  // End recursion of no head
  if (! head) {
      return {regular: regular, actual: actual, modified: modified, id: id, type: type, msg: msg};
  }

  // If the branch is regularly closed, we don't need to do anything else.
  if (isClosed(actualH)) {
    return {regular: regular, actual: closed(), modified: false};
  }

  // If the closing says the branch should close, we also don't need to do
  // anything further, but the hours should be reported as modified
  if (shouldClose(actualH, head)) {
    return {
      regular: regular,
      actual: closed(),
      modified: true,
      id: head.id,
      type: 'closed',
      msg: head.closed_for,
    };
  }

  // If it's an early closing, set closing time to the start of the closing,
  // set the modified flag and recurse
  if (shouldCloseEarly(actualH, head)) {
    return calculateClosings(
      {
        regular: regular,
        actual: {open: actualH.open, close: head.start},
        modified: true,
        id: head.id,
        type: 'closing early',
        msg: head.closed_for,
      },
      tail
    );
  }

  // If it's a late opening, set opening time to the end of the closing,
  // set the modified flag and recurse
  if (shouldOpenLate(actualH, head)) {
    return calculateClosings(
      {
        regular: regular,
        actual: {open: head.end, close: actualH.close},
        modified: true,
        id: head.id,
        type: 'opening late',
        msg: head.closed_for,
      },
      tail
    );
  }

  // recurse
  return calculateClosings(
    {
      regular: regular,
      actual: actualH,
      modified: modified,
      id: id,
      type: type,
      msg: msg,
    },
    tail
  );
}

/**
 * Combine a date a open/close times into full Datetimes.
 * @param {Object} date -  A Moment date object
 * @param {Object} hours - An object containing open/close times
 * @param {String} hours.open - The opening time in HH:MM format
 * @param {String} hours.close - The closing time in HH:MM format
 * @return {Object} An object containing full open/close datetimes
 * @property {Object} open Opening datetime as a Moment object
 * @property {Object} close Closing datetime as a Moment object
 */
function toDatetimes(date, hours) {
  return {
    open: fullDateTime(date, hours.open),
    close: fullDateTime(date, hours.close),
  };
}

/**
 * Generate hours, including closings.
 *
 * All times are Moment.js objects. A full day closing (regular or otherwise) 
 * is represented as a hours object with open and close properties both null.
 * 
 * @param {Object} param - A parameter hash.
 * @param {Array} param.dates - An array of dates whose hours are being
 *   requested.
 * @param {Array} param.regular - An array of regular hours for the branch in
 *  the format returned by the Refinery.
 * @param {Array} param.closings - An array of closings alerts in the format
 *   returned by the Refinery. Defaults to an empty array if not provided.
 * @return {Object} An array of hashes containing the following properties
 * @property {string} orig The date string as originally passed.
 * @property {Object} parsed A momentjs object version of the date.
 * @property {number} dow An integer representing the day of the week
 *   corresponding to the date (0 = Sunday, 6 = Saturday).
 * @property {Object} hours An object with the following proprties:
 * @property {Object} hours.regular An object representing the regular
 *   operating hours of the branch.
 * @property {Object} hours.regular.open The regular opening time.
 * @property {Object} hours.regular.close The regular closing time.
 * @property {Object} hours.actual An object representing the actual operating
 *   hours for the requested date. If there is no exceptional closing, the 
 *   actual hours will match the regular hours and the modified property will 
 *   be false.
 * @property {Object} hours.actual.open The actual opening time.
 * @property {Object} hours.actual.close The actual closing time.
 * @property {Boolean} hours.modified true if the actual and regular hours 
 *   differ
 * @property {String} hours.id Set to the id of the closing by which the
 *   regular hours have been modified, if any.
 * @property {String} hours.msg Message for the closing by which the regular
 *   hours have been modified.
 * @property {String} hours.type The type of closing ('closed', 'closing early',
 *   'opening late').
 * @throws {Error} when required members of parameter hash are not present.
 *
 */
export function branchHours({dates, regular, closings: closings = []} = {}) {
  if (! dates) {
    throw new Error('\'dates\' member is required');
  }

  if (! regular) {
    throw new Error('\'regular\' member is required');
  }

  const parsedClosings = closings.map((c) => parseClosing(c));

  const asDates = dates.map((d) => {
    const parsed = moment(d);
    const dow = parsed.day();
    const regularHours = toDatetimes(parsed, regular[dow]);
    const modifiedHours = calculateClosings(
        {regular: regularHours}, parsedClosings
    );

    return {
      dow: dow,
      orig: d,
      hours: modifiedHours,
    };
  });

  return asDates;
}
