import { should } from "chai";
import moment from 'moment';
import { branchHours } from "../lib/hours.js";

should();

const lpaHours = [
  {
    day: "Sun.",
    open: null,
    close: null
  },
  {
    day: "Mon.",
    open: "10:30",
    close: "20:00"
  },
  {
    day: "Tue.",
    open: "10:30",
    close: "18:00"
  },
  {
    day: "Wed.",
    open: "10:30",
    close: "18:00"
  },
  {
    day: "Thu.",
    open: "10:30",
    close: "20:00"
  },
  {
    day: "Fri.",
    open: "10:30",
    close: "18:00"
  },
  {
    day: "Sat.",
    open: "10:30",
    close: "18:00"
  }
];

const xmas = {
  id: "283895",
  scope: "all",
  _links: {
    web: {
      href: "http://www.nypl.org/node/283895"
    }
  },
  msg: "<p>All NYPL locations will close at 3 p.m. on December 24 and will remain closed on December 25.</p> ",
  display: {
    start: "2015-12-17T00:00:00-05:00",
    end: "2015-12-25T23:00:00-05:00"
  },
  closed_for: "Closed for Christmas Day",
  applies: {
    start: "2015-12-24T15:00:00-05:00",
    end: "2015-12-25T23:00:00-05:00"
  }
};

const mlk = {
  "id": "323625",
  "scope": "all",
  "_links": {
    "web": {
      "href": "http://www.nypl.org/node/323625"
    }
  },
  "msg": "<p>The New York Public Library will be closed on Monday, January 18  in observance of Martin Luther King, Jr. Day.</p>\n",
  "display": {
    "start": "2016-01-11T00:00:00-05:00",
    "end": "2016-01-19T00:00:00-05:00"
  },
  "closed_for": "Closed for Martin Luther King, Jr. Day",
  "applies": {
    "start": "2016-01-18T00:00:00-05:00",
    "end": "2016-01-19T00:00:00-05:00"
  }
};

const pres = {
  "id": "323636",
  "scope": "all",
  "_links": {
    "web": {
      "href": "http://www.nypl.org/node/323636"
    }
  },
  "msg": "<p>The New York Public Library will be closed on Monday, February 15 in observance of Presidents' Day.</p>\n",
  "display": {
    "start": "2016-02-08T00:00:00-05:00",
    "end": "2016-02-16T00:00:00-05:00"
  },
  "closed_for": "Closed for Presidents' Day",
  "applies": {
    "start": "2016-02-15T00:00:00-05:00",
    "end": "2016-02-16T00:00:00-05:00"
  }
};

const dec = branchHours({dates: ['2015-12-23', '2015-12-24', '2015-12-25'], regular: lpaHours, closings: [xmas, mlk, pres]});

describe('hours', () => {
  describe('Any date', () => {
    it('returns the original, unparsed date', () => {
      dec[1].orig.should.equal('2015-12-24');
    });

    it ('returns regular opening time as a Moment date', () => {
      moment.isMoment(dec[0].hours.regular.open).should.eq(true);
    });

    it ('returns regular closing time as a Moment date', () => {
      moment.isMoment(dec[0].hours.regular.close).should.eq(true);
    });

    it('returns the day of the week', () => {
      dec[1].dow.should.equal(4);
    });
  });

  /**
   * Simplest Case:
   * 
   * If there is no modification to the hours, the modified flag should be 
   * true and the "modified" hours should equal the regular hours.
   */
  describe('A day with no modified hours', () => {
    it('does not set the modified flag', () => {
      dec[0].hours.modified.should.be.false;
    });

    it('has the same regular and "modified" hours', () => {
      const reg = dec[0].hours.regular;
      const mod = dec[0].hours.actual;
      reg.open.isSame(mod.open).should.be.true;
    });
  });

  /**
   * All day closing:
   * 
   * Modified hours should null for both opening and closing
   */
  describe('An all-day closing', () => {
    it('sets the modified flag', () => {
      dec[2].hours.modified.should.be.true;
    });

    it('has null hours', () => {
      (dec[2].hours.actual.open === null).should.be.true;
      (dec[2].hours.actual.close === null).should.be.true;
    });
  });

  /**
   * Early closing:
   *
   * For a partial closing, the hours will not be null, and the modified flag
   * will be set
   */
  describe('An early closing', () => {
    it('sets the modified flag', () => {
      dec[1].hours.modified.should.be.true;
    });

    it('has an earlier close than normal', () => {
      const reg = dec[1].hours.regular;
      const mod = dec[1].hours.actual;
      reg.close.hour().should.equal(20); // regularly Close at 8pm
      mod.close.hour().should.equal(15); // close at 3pm instead
    });
  });

  describe('Input sanity checks', () => {
    it('Throws an error if no dates member is provided', () => {
      (() => {
        branchHours({});
      }).should.throw('\'dates\' member is required');
    });

    it('Throws an error if no regular member is provided', () => {
      (() => {
        branchHours({dates: []});
      }).should.throw('\'regular\' member is required');
    });
  });
});
